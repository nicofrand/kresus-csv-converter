# How to use this converter:

1. Export an unencrypted JSON file from your Kresus (in *Backup & restore* > Export)
1. Run `node main.js "your-export.json" > csv-export.csv`
