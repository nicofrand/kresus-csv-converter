const fs = require('fs');
const path = require('path');
const { exit } = require('process');
    filePath = path.join(__dirname, 'start.html');

const COLUMNS_DELIMITER = ';';
const ROWS_DELIMITER = '\n';

function quotesWrapper(text) {
    return `"${text}"`;
}

function formatDate(date) {
    if (date instanceof Date) {
        return date.toISOString().substr(0, 10);
    }

    if (typeof date === 'string') {
        return date;
    }

    return '';
}

function kresusToCSV(data) {
    const accessesNamesMap = new Map();
    for (const access of data.accesses) {
        if (!accessesNamesMap.has(access.vendorId)) {
            accessesNamesMap.set(access.vendorId, access.customLabel || access.label);
        }
    }

    const accountsMap = new Map();
    for (const account of data.accounts) {
        const accountKey = account.id.toString();

        if (!accountsMap.has(accountKey)) {
            accountsMap.set(accountKey, account);
        }
    }

    const categoriesNamesMap = new Map();
    for (const category of data.categories) {
        const categoryKey = category.id.toString();
        if (!categoriesNamesMap.has(categoryKey)) {
            categoriesNamesMap.set(categoryKey, category.label);
        }
    }

    const rows = data.operations.map(transaction => {
        const account = accountsMap.get(transaction.accountId.toString());
        if (typeof account === 'undefined') {
            return [];
        }

        return [
            // Access name
            account ? accessesNamesMap.get(account.vendorId) || '' : '',

            // Account
            account.customLabel || account.label || '',

            // Type
            transaction.type || '',

            // Label
            transaction.customLabel || transaction.label || '',

            // Raw label
            transaction.rawLabel || '',

            // Date
            formatDate(transaction.date),

            // Debit date
            formatDate(transaction.debitDate),

            // Amount
            transaction.amount,

            // Category
            typeof transaction.categoryId === 'number'
                ? categoriesNamesMap.get(transaction.categoryId.toString()) || ''
                : '',
        ];
    });

    const columns = [
        'Bank',
        'Account',
        'Type',
        'Label',
        'Raw label',
        'Date',
        'Debit date',
        'Amount',
        'Category',
    ];
    return `${columns.map(quotesWrapper).join(COLUMNS_DELIMITER)}
${rows
    .map(row => {
        return `${row.map(quotesWrapper).join(COLUMNS_DELIMITER)}`;
    })
    .join(ROWS_DELIMITER)}`;
}

const pathname = process.argv[2];
const jsonContent = fs.readFileSync(pathname, { encoding: 'utf8' });

let data = null;
try
{
    data = JSON.parse(jsonContent);
}
catch (e)
{
    console.error(`Failed to parse JSON: ${e.message}`);
}

if (data)
{
    console.log(kresusToCSV(data.data));
}
